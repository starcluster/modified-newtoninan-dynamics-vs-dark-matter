import numpy as np
import matplotlib.pyplot as plt
from scipy.special import ellipk, ellipe, ellipkm1
from scipy.optimize import curve_fit
import scipy.integrate as integrate

# ---Constantes et unités
kmTOparsec = 3.240779e-14
G = 6.6742e-11*(kmTOparsec*1e-3)**3*1.9884e30 # pc^3.m(soleil)^-1.s-2 depuis SI
a_0 = 1.21e-8*kmTOparsec*1e-5 # pc/s^2
arcminTOrad = 0.00029088820866572
galaxy_dist = 9.36e6 #pc

# ---Données observationnelles
radii = np.array(list(map(float, open('radii.dat').readlines())))
radii = radii*arcminTOrad*galaxy_dist #pc
print("rayons mat visible :\n", radii)

radii_gaz = np.array(list(map(float, open('r_gaz.dat').readlines())))
radii_gaz = radii_gaz*arcminTOrad*galaxy_dist #pc
print("rayons mat gaz :\n", radii_gaz)

sigma = np.array(list(map(float, open('sigma.dat').readlines())))
sigma_brut = np.copy(sigma)
# Correction liée aux major & minor axis
n=len(sigma)
for i in range(0,n-12):
    sigma[i]= sigma[i] - (sigma[i]-sigma[0])*.35
print("matière visible (Lsol/pc^2) : \n",10**(1/2.5*(4.42 + 21.572 - sigma)))

sigma_gaz = np.array(list(map(float, open('s_gaz.dat').readlines())))
print("matière gaz (msol/pc^2):\n",sigma_gaz*1.3)

R_Vr = np.array(list(map(float, open('r.dat').readlines()))) # arcmin
R_Vr = R_Vr*arcminTOrad*galaxy_dist #pc
v_Vr = np.array(list(map(float, open('v.dat').readlines()))) # km/s
v_Vr = v_Vr*kmTOparsec # pc/s
verr_Vr = np.array(list(map(float, open('v_err.dat').readlines()))) # km/s
verr_Vr = verr_Vr*kmTOparsec # pc/s
print("rayons V(r) :\n",R_Vr)
print("V(r) :\n",v_Vr)

def aff_data():
    fig, ax = plt.subplots(2,2)
    ax[0][0].plot(radii/1000,sigma,'+',label='moyenne ajustée')
    ax[0][0].plot(radii/1000,sigma_brut,'+',label='moyenne brute')
    ax[0][0].legend()
    ax[0][0].set_xlabel("rayon (kpc)")
    ax[0][0].set_ylabel("S (mag/arcsec²)")
    ax[0][0].set_title("Brillance de surface NGC3198 (matière visible) - bande R ")
    ax[1][0].plot(radii/1000,10**(1/2.5*(4.42 + 21.572 - sigma)),'+')
    ax[1][0].set_xlabel("rayon (kpc)")
    ax[1][0].set_ylabel("L ($L_\odot/pc^2$)")
    ax[1][0].set_title("luminosité de surface NGC3198 (matière visible) - bande R ")
    ax[0][1].plot(radii_gaz/1000,sigma_gaz,'+')
    ax[0][1].set_xlabel("rayon (kpc)")
    ax[0][1].set_ylabel("$\sigma_{HI}$ ($M_\odot/pc^2$)")
    ax[0][1].set_title("Masse surfacique d'hydrogène neutre NGC3198 (gaz) - pic 21cm")
    ax[1][1].errorbar(R_Vr/1000,v_Vr/kmTOparsec,yerr=verr_Vr/kmTOparsec,fmt='o')
    ax[1][1].set_xlabel("rayon (kpc)")
    ax[1][1].set_ylabel("V(r) (km/s)")
    ax[1][1].set_title("Courbe de rotation NGC3198")
    # ~ plt.tight_layout()
    plt.show()


# ---Fonctions
def gradU(x, y, a, sigma_s, x_n=0):
    """
    Pour un disque massif de rayon a
    Composante en x_n du gradient U tel que F_gravit/m = - grad(U)
    """
    r = np.sqrt(x**2 + y**2)
    p = a + r
    q = a - r
    k = np.sqrt(1-q**2/p**2)
    if k > 1:
        print("k n'est pas bien définie, arret.")
        exit()
    if x_n == 0:
        t1 = 2*x*(r+a)/r**2
    elif x_n ==1:
        t1 = 2*y*(r+a)/r**2
    else:
        print("Composante invalide, arret.")
        exit()
    if k != 1 :
        t2 = (1-0.5*k**2)*ellipk(k) - ellipe(k)
    else :
        k = 1-10e-60
        t2 = (1-0.5*k**2)*ellipkm1(k) - ellipkm1(k)
    if t2 == np.inf :
        print(t2,k)
    return -G*sigma_s*t1*t2

def gradUbis(r,a,sigma_s):
    k = np.sqrt(4*r*a/(r+a)**2)
    if k > 1 :
        print("k n'est pas bien définie, arret.")
        exit()
    t1 = 2/(r*(a+r))
    if k != 1 :
        t2 = -(r**2+a**2)*ellipk(k) + (r**2+a**2 + k**2/(1-k**2)*(a-r)**2/2)*ellipe(k)
    else :
        k = 1-10e-60
        t2 = -(r**2+a**2)*ellipkm1(k) + (r**2+a**2 + k**2/(1-k**2)*(a-r)**2/2)*ellipem1(k)
    if t2 == np.inf :
        print(t2,k)
    return -G*sigma_s*t1*t2
    
gradU_vectorized = np.vectorize(gradU)

def elliptics():
    # Tracer les intégrales elliptiques
    k = np.linspace(-1, 1, 1000)
    y = ellipk(k)
    y2 = ellipe(k)
    plt.plot(k,y)
    plt.plot(k,y2)
    plt.show()


def debug_step_by_step():
    """
    Confirmation que la partie (1-0.5k**2... se comporte bien en 1/sqrt(r) ce qui contredit la théorie !
    """
    r = np.linspace(1, 1000)
    y_th = 1/r
    y_sqrt = 1/np.sqrt(r)

    p = 1 + r
    q = 1 - r
    k = np.sqrt(1-q**2/p**2)

    ellip = (1-0.5*k**2)*ellipk(k) - ellipe(k)
    
    plt.plot(r, y_th, label="theorie")
    plt.plot(r, y_sqrt, label="theorie")
    plt.plot(r, k, label="k")
    plt.plot(r, ellip, label="ellip")
    
    plt.legend()
    plt.loglog()
    plt.show()

def kk(a, r):
    return 2*np.sqrt(a*r)/(a+r)

def h(a, r):
    """
    g is a bounded, slowing varying function of the radius.
    """
    return 1/(1 + np.exp(0.00001*(a-r)))

# a = np.linspace(0,10,100)
# y = h(a, 5)
# plt.plot(a,y)
# plt.show()
    
def kk_lambda(a, r):
    l = 1*h(a,r)*0
    l = 10
    return 2*np.sqrt(a*r)/np.sqrt((a+r)**2+l**2)



def kk_prime(a, r):
    return (np.sqrt(a/r)*(a+r) - 2*np.sqrt(r))/((a+r)**2)

def kk_prime_lambda(a, r):
    l = 1*h(a,r)*0
    l = 10
    s = (a+r)**2+l**2
    return 2*np.sqrt(a)*(np.sqrt(s)/2/np.sqrt(r) - (a+r)/(np.sqrt(s)))/s

def ellipk_prime(k):
    """
    https://math.stackexchange.com/questions/3294919/is-the-derivative-of-the-elliptic-integral-of-the-first-kind-considered-a-mathem
    """
    if k>=1: print("probleme")
    return ellipe(k)/(1-k**2)/k - ellipk(k)/k
    
    #return integrate.romberg(lambda t: k*np.sin(t)**2*(1-k**2*np.sin(t)**2)**(-3/2), 0, np.pi/2, divmax = 10)
    
def psi(r, radius):
    """
    Nouvelle expression de la fonction potentiel psi d'après
    https://www.aanda.org/articles/aa/pdf/2011/07/aa15854-10.pdf
    """
    sigma = 1
    #R = np.linspace(1, 100, 1000)
    #I = np.array([integrate.romberg(lambda a: np.sqrt(a/r)*sigma*kk(a,r)*ellipk(kk(a,r)), 0, 10) for r in R])
    I = integrate.romberg(lambda a: np.sqrt(a/r)*sigma*kk(a,r)*ellipk(kk(a,r)), 0, radius)

    return I

def E(k):
    return ellipk(k)
    if k == 1:
        k = 1+10e-30
        return ellipkm1(k)
    else:
        return ellipk(k)

def gradUtris(r, radius, sigma):
    print(r)
    #sigma = 1
    if r <= radius*3:
        i1 = lambda x: 1/2/np.sqrt(x*r)*kk_lambda(x, r)*E(kk_lambda(x, r))
        i2 = lambda x: np.sqrt(x/r)*kk_prime_lambda(x, r)*E(kk(x, r))
        i3 = lambda x: np.sqrt(x/r)*kk_lambda(x, r)*kk_prime(x,r)*ellipk_prime(kk_lambda(x, r))
    else:
        i1 = lambda x: 1/2/np.sqrt(x*r)*kk(x, r)*E(kk(x, r))
        i2 = lambda x: np.sqrt(x/r)*kk_prime(x, r)*E(kk(x, r))
        i3 = lambda x: np.sqrt(x/r)*kk(x, r)*kk_prime(x,r)*ellipk_prime(kk(x, r))

    delta = 3
    return integrate.romberg(lambda x: (i1(x) + i2(x) + i3(x))*sigma, 0.1, radius, divmax=11)
##+ integrate.romberg(lambda x: (i1(x) + i2(x) + i3(x))*sigma, r+delta, radius, divmax=11)
                            
# ~ print(gradUtris(10, 1))

def Vtris(r, radius, sig):
    g_n_val = gradUtris(r, radius)
    return np.power(((r*g_n_val)**2)/2 + r**2*g_n_val*a_0*np.sqrt((g_n_val/a_0)**2 + 4)/2,1/4)

Vtris_vectorized = np.vectorize(Vtris)

# print(Vtris(1,10), "Vtris")
# exit()
# ~ R = np.linspace(1, 100, 100)
# ~ y = Vtris_vectorized(R,10)
# ~ plt.scatter(R, y, label="V(r)", marker = '+', )
# ~ plt.legend()
# ~ plt.show()
    
def delta_gradU(x, y, a, b, sigma_s, x_n=0):
    return gradU(x, y, b, sigma_s, x_n) - gradU(x, y, a, sigma_s, x_n)

def delta_gradUtris(x, y, a, b, sigma_s, x_n=0):
    return gradUtris(x, b, sigma_s) - gradUtris(x,  a, sigma_s)

delta_gradU_vectorized = np.vectorize(delta_gradU)
delta_gradUtris_vectorized = np.vectorize(delta_gradUtris)

def g_i(x, y, a, b, sigma_s):
    return np.array((-delta_gradU(x, y, a, b, sigma_s, x_n=0), -delta_gradU(x, y, a, b, sigma_s, x_n=1)))

def g_n(r, M_L):
    n = np.shape(radii)[0]
    x,y = r,0 #radii[1]/2,0
    g = 0
    for i in range(0, n):
        s = M_L*10**(1/2.5*(4.42 + 21.572 - sigma[i]))
        if i==0 :
            rmin=radii[0]
            rmax=(radii[1]+radii[0])/2
        elif i==n-1 :
            rmax = radii[-1]
            rmin = (radii[-1]+radii[-2])/2
        else :
            rmin = (radii[i-1]+radii[i])/2
            rmax = (radii[i]+radii[i+1])/2
        g += g_i(x, y, rmin, rmax, s)
        #x = (radii[i]+radii[i-1])/2
    #print("g_vis(",r,")=",g,"\nabs(g)=",abs(g))
    return np.linalg.norm(g)
g_n = np.vectorize(g_n)

def g_n_gaz(r):
    n = np.shape(radii_gaz)[0]
    x,y = r,0 #radii_gaz[1]/2,0
    g = 0
    for i in range(0, n):
        s = 1.3*sigma_gaz[i]
        if i==0 :
            rmin=radii_gaz[0]
            rmax=(radii_gaz[1]+radii_gaz[0])/2
        elif i==n-1 :
            rmax = radii_gaz[-1]
            rmin = (radii_gaz[-1]+radii_gaz[-2])/2
        else :
            rmin = (radii_gaz[i-1]+radii_gaz[i])/2
            rmax = (radii_gaz[i]+radii_gaz[i+1])/2
        g += g_i(x, y, rmin, rmax, s)
    #print("g_gaz(",r,")=",g,"\nabs(g)=",abs(g))
    return np.linalg.norm(g)
g_n_gaz = np.vectorize(g_n_gaz)

def g_n_tot(r, M_L):
    return g_n(r, M_L) + g_n_gaz(r)

def V(r, M_L):
    g_n_val = g_n_tot(r, M_L)
    # return np.power(r**2*a_0*(g_n_val + np.sqrt(g_n_val**2 + 4*g_n_val*a_0)), 1/4) faux ?
    return np.power(((r*g_n_val)**2)/2 + r**2*g_n_val*a_0*np.sqrt((g_n_val/a_0)**2 + 4)/2,1/4)

V_for_fit=np.vectorize(V)

def V_newt(r, M_L):
    g_n_val = g_n_tot(r, M_L)
    return np.sqrt(r*g_n_val)
V_newt=np.vectorize(V_newt)

def V_newtVis(r, M_L):
    g_n_val = g_n(r, M_L)
    return np.sqrt(r*g_n_val)
V_newtVis=np.vectorize(V_newtVis)

def V_newtGaz(r):
    g_n_val = g_n_gaz(r)
    return np.sqrt(r*g_n_val)
V_newtGaz=np.vectorize(V_newtGaz)

def fit():
    print("V(",R_Vr[20],") (km.s-1)  = ",V(R_Vr[20],2.55)/kmTOparsec)
    popt, pcov = curve_fit(V_for_fit, R_Vr, v_Vr, p0=2., method='lm')
    print("M/L_fit = ",popt," +- ", pcov)
    return float(popt),float(pcov)

def debug_gradU():
    # potentiel d'un anneau de sigma=10msol/pc^2 et rayon 0.5-1kpc
    rmin=0.001
    rmax=1000
    sigma=10
    rTab = np.array([10**(i/60) for i in range(100,300)])
    gradUval= delta_gradU_vectorized(rTab,0,500,rmax,sigma,0)
    fig, ax = plt.subplots(1)
    ax.plot(rTab/1000,-gradUval,label="g(r)")
    ax.legend()
    ax.loglog()
    plt.autoscale()
    ax.set_xlabel("rayon (kpc)")
    ax.set_ylabel("g_N(r) (pc/s^2)")
    ax.set_title("g_Newt(r) 'disk toy model'")
    plt.show()

def debug_g_n():
    fig, ax = plt.subplots(1)
    R_Vr_infini = np.array([10**(i/60) for i in range(50,400)])
    gNgaz_graph = g_n_gaz(R_Vr_infini)# pc/s^2
    ax.plot(R_Vr_infini/1000,gNgaz_graph,label="g_N_gaz")
    gNvis_graph = g_n(R_Vr_infini,2.55) # pc/s^2
    ax.plot(R_Vr_infini/1000,gNvis_graph,label="g_N_vis")
    ax.legend()
    ax.loglog()
    ax.set_xlabel("rayon (kpc)")
    ax.set_ylabel("g_N(r) (pc/s^2)")
    ax.set_title("g_Newt(r)")
    plt.show()

def debug_rotation_curve(M_L=2.55):
    fig, ax = plt.subplots(1)
    #R_Vr_infini = np.array([i for i in range(1,60000,1000)])
    Vgraph = V_for_fit(R_Vr,M_L)/kmTOparsec # km/s
    Vgraph_newt = V_newt(R_Vr,M_L)/kmTOparsec # km/s
    Vgraph_newtGaz = V_newtGaz(R_Vr)/kmTOparsec # km/s
    Vgraph_newtVis = V_newtVis(R_Vr,M_L)/kmTOparsec # km/s
    ax.plot(R_Vr/1000,Vgraph,label="MOND ($M/L$="+str((M_L*1e3//1)/1e3)+"$M/L_\odot$)")
    ax.plot(R_Vr/1000,Vgraph_newt,label="Newt_tot")
    ax.plot(R_Vr/1000,Vgraph_newtGaz,label="Newt_gaz")
    ax.plot(R_Vr/1000,Vgraph_newtVis,label="Newt_visible")
    ax.errorbar(R_Vr/1000,v_Vr/kmTOparsec,yerr=verr_Vr/kmTOparsec,fmt='o',label="données")
    ax.legend()
    ax.set_xlabel("rayon (kpc)")
    ax.set_ylabel("vitesse V(r) (km/s)")
    ax.set_title("Courbe de rotation NGC3198")
    plt.show() # x en kpc, y en km/s

def masse_obs(rTab,sigmaTab):
    # rTab en pc ; sigmaTab en m(soleil)/pc^2
    M=0
    n=len(rTab)
    for i in range(0,n):
        if i==0 :
            rmin=rTab[0]
            rmax=(rTab[1]+rTab[0])/2
        elif i==n-1 :
            rmax = rTab[-1]
            rmin = (rTab[-1]+rTab[-2])/2
        else :
            rmin = (rTab[i-1]+rTab[i])/2
            rmax = (rTab[i]+rTab[i+1])/2
        surf = np.pi*(rmax**2-rmin**2)
        M += sigmaTab[i]*surf
    return M # unités de m(soleil)

def debug_masse():
    # masse totale prédite par V(r) asymptotique observé, eq(7) article
    M_asymp = np.mean(v_Vr[-4:-1])**4/G/a_0 # en unités de m(soleil)
    print("Masse déduite de la courbe de rotation :\nM_NGC3198_asymp = ",M_asymp," M_o")
    M_obs_gaz = masse_obs(radii_gaz,sigma_gaz*1.3) # m(soleil)
    print("Masse observée de gaz :\nM_NGC3198_HI*1.3 = ",M_obs_gaz," M_o")
    M_obs_vis = masse_obs(radii,2.55*10**(1/2.5*(4.42 + 21.572 - sigma))) # m(soleil)
    print("Masse observée visible (M/L=2.55) :\nM_NGC3198_vis = ",M_obs_vis," M_o")
    
    
def main():
    # ~ aff_data()
    debug_masse()
    debug_gradU()
    debug_g_n()
    #psi()
    M_L,M_Lerr=fit()
    debug_rotation_curve(M_L)
	
main()
