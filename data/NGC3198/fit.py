import numpy as np
import matplotlib.pyplot as plt
from scipy.special import ellipk, ellipe, ellipkm1
from scipy.optimize import curve_fit
import scipy.integrate as integrate

# ---Constantes et unités
kmTOparsec = 3.240779e-14
G = 6.6742e-11*(kmTOparsec*1e-3)**3*1.9884e30 # pc^3.m(soleil)^-1.s-2 depuis SI
a_0 = 1.21e-8*kmTOparsec*1e-5 # pc/s^2
arcminTOrad = 0.00029088820866572
galaxy_dist = 9.36e6 #pc

radii = np.array(list(map(float, open('radii.dat').readlines())))
radii = radii*arcminTOrad*galaxy_dist #pc

radii_gaz = np.array(list(map(float, open('r_gaz.dat').readlines())))
radii_gaz = radii_gaz*arcminTOrad*galaxy_dist #pc

sigma = np.array(list(map(float, open('sigma.dat').readlines())))
sigma_brut = np.copy(sigma)

sigma_gaz = np.array(list(map(float, open('s_gaz.dat').readlines())))

R_Vr = np.array(list(map(float, open('r.dat').readlines()))) # arcmin
R_Vr = R_Vr*arcminTOrad*galaxy_dist #pc
v_Vr = np.array(list(map(float, open('v.dat').readlines()))) # km/s
v_Vr = v_Vr*kmTOparsec # pc/s

def kk(a, r):
    return 2*np.sqrt(a*r)/(a+r)

def kk_lambda(a, r, l):
    return 2*np.sqrt(a*r)/np.sqrt((a+r)**2+l**2)

def kk_prime(a, r):
    return (np.sqrt(a/r)*(a+r) - 2*np.sqrt(r))/((a+r)**2)

def kk_prime_lambda(a, r, l):
    s = (a+r)**2+l**2
    return 2*np.sqrt(a)*(np.sqrt(s)/2/np.sqrt(r) - (a+r)/(np.sqrt(s)))/s

def K(k):
    return ellipk(k)

def E(k):
    return ellipe(k)

def K_prime(k):
    """
    https://math.stackexchange.com/questions/3294919/is-the-derivative-of-the-elliptic-integral-of-the-first-kind-considered-a-mathem
    """
    return E(k)/(1-k**2)/k - K(k)/k

def gradU(r, radius, sigma, l):
    #print(r)
    if r <= radius*3:
        i1 = lambda x: 1/2/np.sqrt(x*r)*kk_lambda(x, r, l)*E(kk_lambda(x, r, l))
        i2 = lambda x: np.sqrt(x/r)*kk_prime_lambda(x, r, l)*E(kk(x, r))
        i3 = lambda x: np.sqrt(x/r)*kk_lambda(x, r, l)*kk_prime(x,r)*K_prime(kk_lambda(x, r, l))
    else:
        i1 = lambda x: 1/2/np.sqrt(x*r)*kk(x, r)*E(kk(x, r))
        i2 = lambda x: np.sqrt(x/r)*kk_prime(x, r)*E(kk(x, r))
        i3 = lambda x: np.sqrt(x/r)*kk(x, r)*kk_prime(x,r)*K_prime(kk(x, r))

    return integrate.quad(lambda x: (i1(x) + i2(x) + i3(x))*sigma, 0.1, radius)[0]

def delta_gradU(r, a, b, sigma, l):
    return gradU(r, b, sigma, l) - gradU(r, a, sigma, l)

def g_n(r, M_L, l):
    n = np.shape(radii)[0]
    g = 0
    for i in range(0, n):
        s = M_L*10**(1/2.5*(4.42 + 21.572 - sigma[i]))
        if i==0 :
            rmin=radii[0]
            rmax=(radii[1]+radii[0])/2
        elif i==n-1 :
            rmax = radii[-1]
            rmin = (radii[-1]+radii[-2])/2
        else :
            rmin = (radii[i-1]+radii[i])/2
            rmax = (radii[i]+radii[i+1])/2
        g += delta_gradU(r, rmin, rmax, s, l)
    return g

def g_n_gaz(r, l):
    n = np.shape(radii_gaz)[0]
    g = 0
    for i in range(0, n):
        s = 1.3*sigma_gaz[i]
        if i==0 :
            rmin=radii_gaz[0]
            rmax=(radii_gaz[1]+radii_gaz[0])/2
        elif i==n-1 :
            rmax = radii_gaz[-1]
            rmin = (radii_gaz[-1]+radii_gaz[-2])/2
        else :
            rmin = (radii_gaz[i-1]+radii_gaz[i])/2
            rmax = (radii_gaz[i]+radii_gaz[i+1])/2
        g += delta_gradU(r, rmin, rmax, s, l)
    #print("g_gaz(",r,")=",g,"\nabs(g)=",abs(g))
    return g

def g_n_tot(r, M_L, l):
    return g_n_gaz(r, l) + g_n(r, M_L, l)

def V(r, M_L, l):
    g_n_val = g_n_tot(r, M_L, l)
    return np.power(((r*g_n_val)**2)/2 + r**2*g_n_val*a_0*np.sqrt((g_n_val/a_0)**2 + 4)/2,1/4)

V_vect=np.vectorize(V)

# y = v_vect(R_Vr, 2.55)
popt, pcov = curve_fit(V_vect, R_Vr, v_Vr, p0=[2., 3000], method='lm')
print(popt, pcov)

plt.plot(R_Vr, y)
plt.show()
