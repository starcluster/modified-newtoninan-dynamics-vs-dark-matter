set title "Temps de vie muon"
set xlabel "Cut"
set ylabel "Temps de vie muon [t]"
set grid
set xrange [0:30]
plot "rc.dat" using 1:2 w l
