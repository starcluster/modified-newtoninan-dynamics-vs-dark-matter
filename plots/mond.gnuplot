set terminal wxt size 1200,800 enhanced font 'Verdana,10' persist
# set terminal latex
set border linewidth 1.5
set style line 1 linecolor rgb '#FF0000' linetype 1 linewidth 2
set style line 2 linecolor rgb '#000080' linetype 1 linewidth 2
set samples 10000
set title "Intensity calculated with concentric ring"
set xlabel "Distance to the peak ['']"
set ylabel "Intensity (normalized)"
set logscale
set terminal pngcairo size 1200,800
alpha = 2
x0 = 50
A=100
f(x) = A/(1+(x/x0)**alpha)
max(a,b)=a>b?a:b
a=-99999999.
plot 'rprofile.dat' using 2:(a=max(a,$4),0/0) notitle, '' using 2:($4/a)
fit f(x) 'rprofile.dat' u 2:4 via alpha, x0, A
print(alpha)
plot  'profile.dat' u 2:4, f(x)
