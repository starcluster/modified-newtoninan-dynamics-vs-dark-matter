# set terminal wxt size 1200,800 enhanced font 'Verdana,10' persist
# set terminal latex
set border linewidth 1.5
set style line 1 linecolor rgb '#FF0000' linetype 1 linewidth 2
set style line 2 linecolor rgb '#000080' linetype 1 linewidth 2
set samples 10000
set title "Intensity calculated with concentric ring"
set xlabel "Distance to center [kpc]"
set ylabel "Velocity [km/s]"
# set logscale
# set terminal pngcairo size 1200,800

# CONSTANT
G = 6.67 * 10**(-11)

# VARIABLE
rho_0 = 10000
r_0 = 200000
r_c = 0.1
A = 1
B = 1
C = 1
D = 1
# FIT
set xrange [0:1]
set yrange [0:250]
#f(x) = sqrt(G*4*pi*r_0**3*rho_0/3/x/(1+(r_0/x)**2))
#f(x) = sqrt(G*4*pi/3/x/(1+(r_c**2/x**2)))

#f(x) = sqrt(G*4*pi*x**2*rho_0/3/(1+(x/r_0)**2))
#f(x) = A*x/(B+(x/r_0)**2)
#f(x) = A*log(x) + B - C*x
#f(x) = A*x/(1+x**2) +C*x/(x+D)
f(x) = ln
fit [0:1] f(x) '../data/S/rc.dat' u 1:2 via A, C, D


#fit [0:2] f(x) '../data/S/rc.dat' u 1:2 via rho_0, r_0
plot '../data/S/rc.dat.2', f(x)

# PLOT
#plot f(x)
